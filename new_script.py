import os
import sentry_sdk

version = os.environ.get("BITBUCKET_COMMIT")
sentry_sdk.init(dsn='https://0084a66234134272bcde5eac27ed863e@o49697.ingest.sentry.io/115550', release=version)

try: 
   blah()
except Exception as err:
   sentry_sdk.capture_exception(err)